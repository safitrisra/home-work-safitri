# create a function that return true if year is a leap year and false if it was not a leap year
def is_leap_year(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False

year = 2024
if is_leap_year(year):
    print(year, "is a leap year")
else:
    print(year, "is not a leap year")