# create a function that print data from array of object input:
def print_data(array):
    for i, obj in enumerate(array):
        print(f"{i+1}. nama: {obj['name']}, usia: {obj['age']}")

data = [
    {
        'name': 'udin',
        'age': 10
    },
    {
        'name': 'ujang',
        'age': 11
    },
    {
        'name': 'asep',
        'age': 12
    }
]

print_data(data)