
# find A
def count_a(word, loops):
    final_string = word * loops
    count = 0
    for letter in final_string:
        if letter == 'a':
            count += 1
    return count
word = "aha"
loops = 3
result = count_a(word, loops)
print(result) # output: 6