# create a function that return random string that could contain lower-case, upper-case, numeric, special character
import random
import string

def random_string(length):
    chars = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(chars) for _ in range(length))

my_string = random_string(10)
print(my_string)

# create a function that split string into array
def split_string(string, delimiter):
    return string.split(delimiter)

input_string = "udin, ujang, asep"
delimiter = ", "
output_array = split_string(input_string, delimiter)
print(output_array)