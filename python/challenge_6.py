
# create a function that return object that contain gruop of people based on sex, age, marital-status, job-status input:
def group_people(people):
    output = {
        "sex": {"male": [], "female": []},
        "age": {"under20": [], "older": []},
        "marriage": {"single": [], "married": []},
        "status": {"student": [], "employee": []}
    }
    for person in people:
        output["sex"][person["sex"]].append(person["name"])
        
        if person["age"] < 20:
            output["age"]["under20"].append(person["name"])
        else:
            output["age"]["older"].append(person["name"])
            
        output["marriage"][person["marital"]].append(person["name"])
        output["status"][person["social"]].append(person["name"])
    
    return output
people = [
    {
        "name":"udin",
        "sex":"male",
        "age":10,
        "marital":"single",
        "social":"student"
    },
    {
        "name":"ujang",
        "sex":"male",
        "age":25,
        "marital":"married",
        "social":"employee"
    },
    {"name":"icih",
        "sex":"female",
        "age":10,
        "marital":"single",
        "social":"student"
    },
    {
        "name":"eneng",
        "sex":"female",
        "age":100,
        "marital":"married",
        "social":"employee"
    },
    {
        "name":"asep",
        "sex":"male",
        "age":20,
        "marital":"single",
        "social":"employee"
    },
]

grouped_people = group_people(people)
print(grouped_people)
